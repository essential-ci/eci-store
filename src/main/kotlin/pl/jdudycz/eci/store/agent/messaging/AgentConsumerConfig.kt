package pl.jdudycz.eci.store.agent.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.*
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class AgentConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun agentRegister(): EventConsumer = EventConsumer.create(AGENT_REGISTER_TOPIC, kafkaProperties)

    @Bean
    fun agentUnregister(): EventConsumer = EventConsumer.create(AGENT_UNREGISTER_TOPIC, kafkaProperties)

    @Bean
    fun agentAvailable(): EventConsumer = EventConsumer.create(AGENT_AVAILABLE_TOPIC, kafkaProperties)

    @Bean
    fun agentUnavailable(): EventConsumer = EventConsumer.create(AGENT_UNAVAILABLE_TOPIC, kafkaProperties)
}
