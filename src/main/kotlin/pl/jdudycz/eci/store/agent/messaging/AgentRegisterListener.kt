package pl.jdudycz.eci.store.agent.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Agent.AgentRegistrationRequest
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.agentRegisterFailed
import pl.jdudycz.eci.common.domain.event.agentRegisterSuccess
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.agent.model.Agent
import pl.jdudycz.eci.store.agent.persistence.AgentRepository
import pl.jdudycz.eci.store.space.persistence.SpaceRepository
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*
import javax.annotation.PostConstruct

@Service
class AgentRegisterListener(@Qualifier("agentRegister") private val consumer: EventConsumer,
                            private val publisher: EventPublisher,
                            private val spaceRepository: SpaceRepository,
                            private val agentRepository: AgentRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume().flatMap(::handleAgentRegister).subscribe()
    }

    private fun handleAgentRegister(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { AgentRegistrationRequest.parseFrom(event.data) }
                    .flatMap { register(event.correlationId.toUUID(), it) }

    private fun register(correlationId: UUID, data: AgentRegistrationRequest): Mono<Void> =
            spaceRepository.findByAgentRegistrationCode(data.registrationCode)
                    .switchIfEmpty(Mono.error(Exception("Registration code is not valid")))
                    .flatMap { reuseOrRegisterAgent(data, it.id!!) }
                    .map {
                        agentRegisterSuccess(correlationId, it.id!!, data)
                    }
                    .onErrorResume {
                        log.debug("Failed to register agent ${data.name} on host ${data.host}: ${it.localizedMessage}")
                        agentRegisterFailed(correlationId, it.localizedMessage, data).toMono()
                    }
                    .transform(publisher::publish)
                    .then()

    private fun reuseOrRegisterAgent(data: AgentRegistrationRequest, spaceId: String) = data.run {
        agentRepository
                .findBySpaceIdAndHost(spaceId, host)
                .flatMap { agentRepository.save(it.copy(apiKey = data.apiKey)) }
                .doOnNext {
                    log.debug("Agent with host \"$host\" already registered to space $spaceId - updating api key to ${it.apiKey}")
                }
                .switchIfEmpty(agentRepository.save(Agent(this, spaceId)).doOnNext {
                    log.debug("Agent with host \"${it.host}\" registered to space ${it.spaceId}, api key: ${it.apiKey}")
                })
    }

    companion object {
        private val log = logger<AgentRegisterListener>()
    }
}
