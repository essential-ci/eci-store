package pl.jdudycz.eci.store.agent.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.store.agent.persistence.AgentRepository
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Service
class AgentUnavailableListener(@Qualifier("agentUnavailable") private val consumer: EventConsumer,
                               private val agentRepository: AgentRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { it.data.toStringUtf8() }
                .flatMap(::markUnavailable)
                .onErrorContinue { e, _ -> log.debug("Failed to mark agent unavailable: ${e.localizedMessage}") }
                .subscribe()
    }

    private fun markUnavailable(agentId: String): Mono<Void> =
            agentRepository
                    .findById(agentId)
                    .flatMap { agentRepository.save(it.copy(isAvailable = false)) }
                    .then()

    companion object {
        private val log = logger<AgentUnavailableListener>()
    }
}
