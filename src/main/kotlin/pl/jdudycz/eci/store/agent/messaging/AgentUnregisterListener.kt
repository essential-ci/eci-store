package pl.jdudycz.eci.store.agent.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.agentUnregistered
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.agent.persistence.AgentUnregisterTransaction
import reactor.core.publisher.Mono
import java.util.*
import javax.annotation.PostConstruct

@Service
class AgentUnregisterListener(@Qualifier("agentUnregister") private val consumer: EventConsumer,
                              private val publisher: EventPublisher,
                              private val agentUnregister: AgentUnregisterTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .flatMap(::handleAgentUnregister)
                .onErrorContinue { e, _ -> log.debug("Failed to unregister agent: ${e.localizedMessage}") }
                .subscribe()
    }

    private fun handleAgentUnregister(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { event.data.toStringUtf8() }
                    .flatMap { unregister(event.correlationId.toUUID(), it) }

    private fun unregister(correlationId: UUID, id: String): Mono<Void> =
            agentUnregister
                    .unregister(id)
                    .then(createUnregisterEvent(correlationId, id))
                    .transform(publisher::publish)
                    .then()

    private fun createUnregisterEvent(correlationId: UUID, id: String) = Mono.fromCallable {
        log.debug("Agent with id $id unregistered")
        agentUnregistered(correlationId, id)
    }

    companion object {
        private val log = logger<AgentRegisterListener>()
    }
}
