package pl.jdudycz.eci.store.agent.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Agent

@Document
data class Agent(
        var id: String?,
        val spaceId: String,
        val host: String,
        val name: String,
        val apiKey: String,
        val isAvailable: Boolean
) {
    constructor(data: Agent.AgentRegistrationRequest, spaceId: String)
            : this(null, spaceId, data.host, data.name, data.apiKey, true)
}
