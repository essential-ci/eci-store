package pl.jdudycz.eci.store.agent.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.agent.model.Agent
import reactor.core.publisher.Mono

@Repository
interface AgentRepository : ReactiveCrudRepository<Agent, String> {

    fun existsByHost(host: String): Mono<Boolean>

    fun findBySpaceIdAndHost(spaceId: String, host: String): Mono<Agent>
}

