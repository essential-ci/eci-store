package pl.jdudycz.eci.store.agent.persistence

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.job.persistence.JobRepository
import pl.jdudycz.eci.store.job.persistence.transactions.JobCancelTransaction
import reactor.core.publisher.Mono

@Service
class AgentUnregisterTransaction(private val agentRepository: AgentRepository,
                                 private val jobCancel: JobCancelTransaction,
                                 private val jobRepository: JobRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun unregister(agentId: String): Mono<Void> =
            jobRepository.findAllByAgentId(agentId)
                    .map { jobCancel.cancel(it.id!!) }
                    .then(agentRepository.deleteById(agentId))
}
