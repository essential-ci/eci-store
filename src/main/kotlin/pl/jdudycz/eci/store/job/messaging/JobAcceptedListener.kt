package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job.AgentsJob
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.doOnEmpty
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.store.job.persistence.JobRepository
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Service
class JobAcceptedListener(@Qualifier("jobAccepted") private val consumer: EventConsumer,
                          private val repository: JobRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { AgentsJob.parseFrom(it.data) }
                .flatMap(::assignAgent)
                .subscribe()
    }

    private fun assignAgent(data: AgentsJob): Mono<Void> =
            repository
                    .findById(data.jobId)
                    .doOnEmpty { log.debug("Couldn't find job with id ${data.jobId}") }
                    .map { it.copy(agentId = data.agentId) }
                    .flatMap(repository::save)
                    .resumeOnError { log.debug("Failed to assign job ${data.jobId} to agent ${data.agentId}: ${it.localizedMessage}") }
                    .then()

    companion object {
        private val log = logger<JobAcceptedListener>()
    }
}
