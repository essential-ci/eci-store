package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.store.job.persistence.transactions.JobCancelTransaction
import javax.annotation.PostConstruct

@Service
class JobCancelledListener(@Qualifier("jobCancelled") private val consumer: EventConsumer,
                           private val jobCancel: JobCancelTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { it.data.toStringUtf8() }
                .flatMap(::cancel)
                .subscribe()
    }

    private fun cancel(jobId: String) =
            jobCancel
                    .cancel(jobId)
                    .resumeOnError { log.debug("Failed to cancel job $jobId: ${it.localizedMessage}") }

    companion object {
        private val log = logger<JobCancelledListener>()
    }
}
