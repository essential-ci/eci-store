package pl.jdudycz.eci.store.job.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.*
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class JobConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun jobCreate(): EventConsumer = EventConsumer.create(JOB_CREATE_TOPIC, kafkaProperties)

    @Bean
    fun jobAccepted(): EventConsumer = EventConsumer.create(JOB_ACCEPTED_TOPIC, kafkaProperties)

    @Bean
    fun jobCancelled(): EventConsumer = EventConsumer.create(JOB_CANCELLED_TOPIC, kafkaProperties)

    @Bean
    fun jobFailed(): EventConsumer = EventConsumer.create(JOB_FAILED_TOPIC, kafkaProperties)

    @Bean
    fun jobSucceeded(): EventConsumer = EventConsumer.create(JOB_SUCCEEDED_TOPIC, kafkaProperties)

    @Bean
    fun jobStarted(): EventConsumer = EventConsumer.create(JOB_STARTED_TOPIC, kafkaProperties)

    @Bean
    fun taskSucceeded(): EventConsumer = EventConsumer.create(TASK_SUCCEEDED_TOPIC, kafkaProperties)

    @Bean
    fun taskFailed(): EventConsumer = EventConsumer.create(TASK_FAILED_TOPIC, kafkaProperties)

    @Bean
    fun taskOutput(): EventConsumer = EventConsumer.create(TASK_OUTPUT_TOPIC, kafkaProperties)

    @Bean
    fun taskStarted(): EventConsumer = EventConsumer.create(TASK_STARTED_TOPIC, kafkaProperties)
}
