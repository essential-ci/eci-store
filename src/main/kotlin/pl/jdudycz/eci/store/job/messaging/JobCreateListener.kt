package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job.JobData
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.jobCreateFailed
import pl.jdudycz.eci.common.domain.event.jobCreated
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.job.persistence.transactions.JobCreateTransaction
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*
import javax.annotation.PostConstruct

@Service
class JobCreateListener(@Qualifier("jobCreate") private val consumer: EventConsumer,
                        private val publisher: EventPublisher,
                        private val jobCreate: JobCreateTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume().flatMap(::handleJobCreate).subscribe()
    }

    private fun handleJobCreate(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { JobData.parseFrom(event.data) }
                    .flatMap { create(event.correlationId.toUUID(), it) }

    private fun create(correlationId: UUID, data: JobData): Mono<Void> =
            jobCreate.create(correlationId, data)
                    .map { (job, tasks) ->
                        log.debug("Job ${job.id} created")
                        jobCreated(correlationId, job.id!!, data, tasks)
                    }
                    .onErrorResume {
                        log.debug("Failed to create job", it)
                        jobCreateFailed(correlationId, it.localizedMessage, data).toMono()
                    }
                    .transform(publisher::publish)
                    .then()

    companion object {
        private val log = logger<JobCreateListener>()
    }
}
