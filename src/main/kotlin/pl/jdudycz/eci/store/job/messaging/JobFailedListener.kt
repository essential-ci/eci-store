package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job.JobFailure
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.store.job.persistence.transactions.JobFailedTransaction
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Service
class JobFailedListener(@Qualifier("jobFailed") private val consumer: EventConsumer,
                        private val jobFailed: JobFailedTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { JobFailure.parseFrom(it.data) }
                .flatMap(::markFailed)
                .subscribe()
    }

    private fun markFailed(jobFailure: JobFailure): Mono<Void> =
            jobFailed
                    .markFailed(jobFailure.jobId, jobFailure.errorMessage)
                    .resumeOnError { log.debug("Failed to fail job ${jobFailure.jobId}: ${it.localizedMessage}") }

    companion object {
        private val log = logger<JobFailedListener>()
    }
}
