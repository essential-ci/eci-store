package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.doOnEmpty
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.store.job.model.ExecutionStatus
import pl.jdudycz.eci.store.job.persistence.JobRepository
import reactor.core.publisher.Mono
import java.time.Instant
import javax.annotation.PostConstruct

@Service
class JobStartedListener(@Qualifier("jobStarted") private val consumer: EventConsumer,
                         private val repository: JobRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { it.data.toStringUtf8() }
                .flatMap(::markRunning)
                .subscribe()
    }

    private fun markRunning(jobId: String): Mono<Void> =
            repository.findById(jobId)
                    .doOnEmpty { log.debug("Couldn't find job with id $jobId") }
                    .map { it.copy(timeStarted = Instant.now(), status = ExecutionStatus.RUNNING) }
                    .flatMap(repository::save)
                    .resumeOnError { log.debug("Failed to mark job $jobId started: ${it.localizedMessage}") }
                    .then()

    companion object {
        private val log = logger<JobStartedListener>()
    }
}
