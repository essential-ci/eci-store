package pl.jdudycz.eci.store.job.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.store.job.persistence.TaskOutputRepository
import pl.jdudycz.eci.store.job.model.TaskOutput
import reactor.core.publisher.Mono
import java.time.Instant
import javax.annotation.PostConstruct

@Service
class TaskOutputListener(@Qualifier("taskOutput") private val consumer: EventConsumer,
                         private val repository: TaskOutputRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume().flatMap(::save).subscribe()
    }

    private fun save(event: EventOuterClass.Event) =
            Mono.fromCallable { Job.TaskOutput.parseFrom(event.data) }
                    .map { TaskOutput(Instant.ofEpochMilli(event.timestamp), it) }
                    .flatMap(repository::save)
                    .resumeOnError { log.debug("Failed to save output: ${it.localizedMessage}") }
                    .then()

    companion object {
        private val log = logger<TaskOutputListener>()
    }
}
