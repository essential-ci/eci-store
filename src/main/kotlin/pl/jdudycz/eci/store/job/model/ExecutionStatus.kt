package pl.jdudycz.eci.store.job.model

enum class ExecutionStatus{
    PENDING,
    SUCCEEDED,
    RUNNING,
    FAILED,
    CANCELLED
}
