package pl.jdudycz.eci.store.job.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Job
import java.time.Instant

@Document
data class Job(
        val id: String?,
        val projectId: String,
        val timeCreated: Instant,
        val timeStarted: Instant?,
        val timeFinished: Instant?,
        val triggerType: TriggerType,
        val agentId: String?,
        val refValue: String,
        val commitSha: String,
        val status: ExecutionStatus,
        val errorMessage: String?) {

    constructor(data: Job.JobData) : this(
            null,
            data.project.id,
            Instant.now(),
            null,
            null,
            TriggerType.valueOf(data.triggerType.toString()),
            null,
            data.change.refValue,
            data.change.checkoutSha,
            ExecutionStatus.PENDING,
            null
    )
}
