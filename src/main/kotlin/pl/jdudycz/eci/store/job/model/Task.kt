package pl.jdudycz.eci.store.job.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Job
import pl.jdudycz.eci.common.util.asEnvMap
import pl.jdudycz.eci.common.util.asEnvString
import java.time.Instant

@Document
data class Task(val id: String?,
                val jobId: String,
                val ordinal: Int,
                val name: String,
                val image: String,
                val variables: Map<String, Any>,
                val secretVariables: Map<String, Any>,
                val commands: List<String>,
                val status: ExecutionStatus,
                val timeStarted: Instant?,
                val timeFinished: Instant?) {

    constructor(jobId: String, task: Job.PipelineTask) : this(
            null,
            jobId,
            task.ordinal,
            task.name,
            task.image,
            task.variablesList.asEnvMap(),
            task.secretVariablesList.asEnvMap(),
            task.commandsList,
            ExecutionStatus.PENDING,
            null,
            null
    )

    fun toPipelineTask(): Job.PipelineTask = Job.PipelineTask.newBuilder()
            .setId(id)
            .setOrdinal(ordinal)
            .setName(name)
            .setImage(image)
            .addAllVariables(variables.asEnvString())
            .addAllSecretVariables(secretVariables.asEnvString())
            .addAllCommands(commands)
            .build()
}
