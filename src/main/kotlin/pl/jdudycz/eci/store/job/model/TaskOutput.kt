package pl.jdudycz.eci.store.job.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Job
import java.time.Instant

@Document
data class TaskOutput(
        val taskId: String,
        val timestamp: Instant,
        val content: String
) {
    constructor(timestamp: Instant, data: Job.TaskOutput) : this(data.taskId, timestamp, data.content)
}
