package pl.jdudycz.eci.store.job.model

enum class TriggerType {
    BRANCH,
    TAG,
}
