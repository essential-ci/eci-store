package pl.jdudycz.eci.store.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.job.model.Job
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface JobRepository : ReactiveCrudRepository<Job, String> {
    fun findAllByProjectId(projectId: String): Flux<Job>

    fun deleteAllByProjectId(projectId: String): Mono<Void>

    fun findAllByAgentId(agentId: String): Flux<Job>
}
