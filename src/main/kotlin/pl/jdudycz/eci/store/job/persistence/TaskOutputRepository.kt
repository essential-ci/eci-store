package pl.jdudycz.eci.store.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.job.model.TaskOutput
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface TaskOutputRepository : ReactiveCrudRepository<TaskOutput, String> {

    fun findAllByTaskId(taskId: String): Flux<TaskOutput>

    fun deleteAllByTaskId(taskId: String): Mono<Void>
}
