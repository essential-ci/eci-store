package pl.jdudycz.eci.store.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.job.model.Task
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface TaskRepository : ReactiveCrudRepository<Task, String> {

    fun findAllByJobId(jobId: String): Flux<Task>

    fun deleteAllByJobId(jobId: String): Mono<Void>

}
