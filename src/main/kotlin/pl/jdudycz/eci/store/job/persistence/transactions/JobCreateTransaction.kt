package pl.jdudycz.eci.store.job.persistence.transactions

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.common.domain.Job.JobData
import pl.jdudycz.eci.common.domain.Job.PipelineTask
import pl.jdudycz.eci.store.job.persistence.JobRepository
import pl.jdudycz.eci.store.job.persistence.TaskRepository
import pl.jdudycz.eci.store.job.model.Job
import pl.jdudycz.eci.store.job.model.Task
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Service
class JobCreateTransaction(private val jobRepository: JobRepository,
                           private val taskRepository: TaskRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun create(correlationId: UUID, data: JobData): Mono<Pair<Job, List<PipelineTask>>> =
            jobRepository.save(Job(data)).flatMap { saveTasks(it, data.tasksList) }

    private fun saveTasks(job: Job, tasks: List<PipelineTask>): Mono<Pair<Job, List<PipelineTask>>> =
            Flux.fromIterable(tasks)
                    .map { Task(job.id!!, it) }
                    .flatMap { taskRepository.save(it) }
                    .collectList()
                    .map { Pair(job, it.map(Task::toPipelineTask)) }
}
