package pl.jdudycz.eci.store.job.persistence.transactions

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.common.util.doOnEmpty
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.store.job.model.ExecutionStatus
import pl.jdudycz.eci.store.job.model.Job
import pl.jdudycz.eci.store.job.model.Task
import pl.jdudycz.eci.store.job.persistence.JobRepository
import pl.jdudycz.eci.store.job.persistence.TaskRepository
import pl.jdudycz.eci.store.job.utils.hasNotCompleted
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Service
class JobFailedTransaction(private val jobRepository: JobRepository,
                           private val taskRepository: TaskRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun markFailed(jobId: String, errorMessage: String): Mono<Void> =
            jobRepository
                    .findById(jobId)
                    .doOnEmpty { log.debug("Couldn't find job with id $jobId") }
                    .transform { markFailed(it, errorMessage) }
                    .thenMany(taskRepository.findAllByJobId(jobId))
                    .transform(::cancelRemainingTasks)
                    .then()

    private fun markFailed(job: Mono<Job>, errorMessage: String) = job
            .map {
                it.copy(timeFinished = Instant.now(),
                        status = ExecutionStatus.FAILED,
                        errorMessage = errorMessage)
            }
            .flatMap { jobRepository.save(it) }


    private fun cancelRemainingTasks(tasks: Flux<Task>) =
            tasks.filter { it.status.hasNotCompleted() }
                    .map { it.copy(timeFinished = Instant.now(), status = ExecutionStatus.CANCELLED) }
                    .flatMap { taskRepository.save(it) }
                    .then()

    companion object {
        private val log = logger<JobCancelTransaction>()
    }
}
