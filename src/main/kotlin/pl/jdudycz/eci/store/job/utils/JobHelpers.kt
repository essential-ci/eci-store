package pl.jdudycz.eci.store.job.utils

import pl.jdudycz.eci.store.job.model.ExecutionStatus

fun ExecutionStatus.hasNotCompleted() =
        this != ExecutionStatus.FAILED && this != ExecutionStatus.SUCCEEDED
