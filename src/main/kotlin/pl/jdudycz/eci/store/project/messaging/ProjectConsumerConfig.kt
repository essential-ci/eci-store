package pl.jdudycz.eci.store.project.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.PROJECT_REMOVE_TOPIC
import pl.jdudycz.eci.common.kafka.PROJECT_SAVE_TOPIC
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class ProjectConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun projectSave(): EventConsumer = EventConsumer.create(PROJECT_SAVE_TOPIC, kafkaProperties)

    @Bean
    fun projectRemove(): EventConsumer = EventConsumer.create(PROJECT_REMOVE_TOPIC, kafkaProperties)
}
