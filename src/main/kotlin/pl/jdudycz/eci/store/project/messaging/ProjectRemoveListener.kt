package pl.jdudycz.eci.store.project.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.common.domain.event.EventOuterClass.Event
import pl.jdudycz.eci.common.domain.event.projectRemoved
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.project.persistence.ProjectRemoveTransaction
import reactor.core.publisher.Mono
import java.util.*
import javax.annotation.PostConstruct

@Service
class ProjectRemoveListener(@Qualifier("projectRemove") private val consumer: EventConsumer,
                            private val publisher: EventPublisher,
                            private val cascadeRemove: ProjectRemoveTransaction) {

    @PostConstruct
    @Transactional
    fun listen() {
        consumer.consume()
                .flatMap(::handleProjectRemove)
                .onErrorContinue { e, _ -> log.debug("Failed to remove project: ${e.localizedMessage}") }
                .subscribe()
    }

    private fun handleProjectRemove(event: Event): Mono<Void> =
            Mono.fromCallable { event.data.toStringUtf8() }
                    .flatMap { remove(event.correlationId.toUUID(), it) }

    fun remove(correlationId: UUID, projectId: String): Mono<Void> =
            cascadeRemove.remove(projectId)
                    .then(createRemoveEvent(correlationId, projectId))
                    .transform(publisher::publish)
                    .then()

    private fun createRemoveEvent(correlationId: UUID, id: String): Mono<Event> = Mono.fromCallable {
        log.debug("Project with id $id removed")
        projectRemoved(correlationId, id)
    }

    companion object {
        private val log = logger<ProjectSaveListener>()
    }
}
