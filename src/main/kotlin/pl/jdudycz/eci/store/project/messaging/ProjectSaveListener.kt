package pl.jdudycz.eci.store.project.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Project.ProjectData
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.projectSaveFailed
import pl.jdudycz.eci.common.domain.event.projectSaveSuccess
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.project.model.Project
import pl.jdudycz.eci.store.project.persistence.ProjectRepository
import pl.jdudycz.eci.store.space.persistence.SpaceRepository
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*
import javax.annotation.PostConstruct

@Service
class ProjectSaveListener(@Qualifier("projectSave") private val consumer: EventConsumer,
                          private val publisher: EventPublisher,
                          private val projectRepository: ProjectRepository,
                          private val spaceRepository: SpaceRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume().flatMap(::handleProjectSave).subscribe()
    }

    private fun handleProjectSave(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { ProjectData.parseFrom(event.data) }
                    .flatMap { save(event.correlationId.toUUID(), it) }

    private fun save(correlationId: UUID, data: ProjectData): Mono<Void> =
            Mono.fromCallable {
                if (data.id.isEmpty()) projectRepository.verifyNameNotTaken(data.spaceId, data.name)
                else spaceRepository.verifyExistsById(data.spaceId)
            }
                    .then(projectRepository.save(Project(data)))
                    .map {
                        log.debug("Project \"${it.name}\" saved")
                        projectSaveSuccess(correlationId, it.id!!, data)
                    }
                    .onErrorResume {
                        log.debug("Failed to create project: ${it.localizedMessage}")
                        projectSaveFailed(correlationId, it.localizedMessage, data).toMono()
                    }
                    .transform(publisher::publish)
                    .then()

    companion object {
        private val log = logger<ProjectSaveListener>()
    }
}
