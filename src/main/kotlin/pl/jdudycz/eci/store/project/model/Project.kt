package pl.jdudycz.eci.store.project.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Project
import pl.jdudycz.eci.common.util.asEnvMap


@Document
data class Project(
        var id: String?,
        val spaceId: String,
        val name: String,
        val repoUrl: String,
        val token: String?,
        val variables: Map<String, Any>,
        val secretVariables: Map<String, Any>
) {
    constructor(data: Project.ProjectData) : this(
            if (data.id.isEmpty()) null else data.id,
            data.spaceId,
            data.name,
            data.repoUrl,
            if (data.isPrivate) data.token else null,
            data.variablesList.asEnvMap(),
            data.secretVariablesList.asEnvMap(),
    )
}
