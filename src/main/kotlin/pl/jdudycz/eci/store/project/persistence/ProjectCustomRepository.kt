package pl.jdudycz.eci.store.project.persistence

import reactor.core.publisher.Mono

interface ProjectCustomRepository {

    fun verifyNameNotTaken(spaceId: String, name: String): Mono<Void>
}
