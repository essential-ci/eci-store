package pl.jdudycz.eci.store.project.persistence

import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
class ProjectCustomRepositoryImpl(@Lazy private val projectRepository: ProjectRepository) : ProjectCustomRepository {

    override fun verifyNameNotTaken(spaceId: String, name: String): Mono<Void> =
            projectRepository.existsBySpaceIdAndName(spaceId, name)
                    .doOnNext { if (it) throw Exception("Repo with name \"${name}\" already exists in this space") }
                    .then()
}
