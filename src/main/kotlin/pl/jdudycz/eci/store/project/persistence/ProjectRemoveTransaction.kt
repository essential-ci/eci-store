package pl.jdudycz.eci.store.project.persistence

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.job.persistence.JobRepository
import pl.jdudycz.eci.store.job.persistence.TaskOutputRepository
import pl.jdudycz.eci.store.job.persistence.TaskRepository
import pl.jdudycz.eci.store.job.model.Job
import pl.jdudycz.eci.store.job.model.Task
import reactor.core.publisher.Mono

@Service
class ProjectRemoveTransaction(private val projectRepository: ProjectRepository,
                               private val jobRepository: JobRepository,
                               private val taskRepository: TaskRepository,
                               private val taskOutputRepository: TaskOutputRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun remove(projectId: String): Mono<Void> =
            jobRepository.findAllByProjectId(projectId)
                    .flatMap { deleteJobWithTasks(it) }
                    .then(projectRepository.deleteById(projectId))

    private fun deleteJobWithTasks(job: Job) =
            taskRepository
                    .findAllByJobId(job.id!!)
                    .flatMap { deleteTaskWithOutput(it) }
                    .then(jobRepository.delete(job))

    private fun deleteTaskWithOutput(task: Task) =
            taskOutputRepository
                    .deleteAllByTaskId(task.id!!)
                    .then(taskRepository.delete(task))

}
