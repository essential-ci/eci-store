package pl.jdudycz.eci.store.project.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.project.model.Project
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
@Suppress("SpringDataRepositoryMethodReturnTypeInspection")
interface ProjectRepository : ReactiveCrudRepository<Project, String>, ProjectCustomRepository {

    fun existsBySpaceIdAndName(spaceId: String, name: String): Mono<Boolean>

    fun findAllBySpaceId(spaceId: String): Flux<Project>
}
