package pl.jdudycz.eci.store.space.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Space
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.store.space.model.SpaceRoleMapping
import pl.jdudycz.eci.store.space.persistence.transactions.AccessGrantTransaction
import javax.annotation.PostConstruct

@Service
class AccessGrantListener(@Qualifier("accessGrant") private val consumer: EventConsumer,
                          private val accessGrant: AccessGrantTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { Space.SpaceRoleMapping.parseFrom(it.data) }
                .flatMap { accessGrant.grantAccess(SpaceRoleMapping(it)) }
                .doOnNext { log.debug("Role mapping saved: $it") }
                .subscribe()
    }

    companion object {
        private val log = logger<SpaceSaveListener>()
    }
}
