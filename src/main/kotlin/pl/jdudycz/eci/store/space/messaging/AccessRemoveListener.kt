package pl.jdudycz.eci.store.space.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Space
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.store.space.persistence.SpaceRoleMappingRepository
import javax.annotation.PostConstruct

@Service
class AccessRemoveListener(@Qualifier("accessRemove") private val consumer: EventConsumer,
                           private val repository: SpaceRoleMappingRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .map { Space.SpaceAccessRemoveRequest.parseFrom(it.data) }
                .flatMap { repository.deleteBySpaceIdAndUserId(it.spaceId, it.userId) }
                .subscribe()
    }
}
