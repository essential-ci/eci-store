package pl.jdudycz.eci.store.space.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.*
import pl.jdudycz.eci.common.kafka.core.EventConsumer

@Configuration
class SpaceConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun spaceSave(): EventConsumer = EventConsumer.create(SPACE_SAVE_TOPIC, kafkaProperties)

    @Bean
    fun spaceRemove(): EventConsumer = EventConsumer.create(SPACE_REMOVE_TOPIC, kafkaProperties)

    @Bean
    fun accessGrant(): EventConsumer = EventConsumer.create(SPACE_ACCESS_GRANT_TOPIC, kafkaProperties)

    @Bean
    fun accessRemove(): EventConsumer = EventConsumer.create(SPACE_ACCESS_REMOVE_TOPIC, kafkaProperties)
}
