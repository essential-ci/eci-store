package pl.jdudycz.eci.store.space.messaging

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.spaceRemoved
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.space.persistence.transactions.SpaceRemoveTransaction
import reactor.core.publisher.Mono
import java.util.*
import javax.annotation.PostConstruct

@Service
class SpaceRemoveListener(@Qualifier("spaceRemove") private val consumer: EventConsumer,
                          private val publisher: EventPublisher,
                          private val spaceRemove: SpaceRemoveTransaction) {

    @PostConstruct
    fun listen() {
        consumer.consume()
                .flatMap(::handleSpaceRemove)
                .onErrorContinue { e, _ -> log.debug("Failed to remove space: ${e.localizedMessage}") }
                .subscribe()
    }

    private fun handleSpaceRemove(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { event.data.toStringUtf8() }
                    .flatMap { remove(event.correlationId.toUUID(), it) }

    private fun remove(correlationId: UUID, id: String) =
            spaceRemove.remove(id)
                    .then(createRemoveEvent(correlationId, id))
                    .transform(publisher::publish)
                    .then()

    private fun createRemoveEvent(correlationId: UUID, id: String): Mono<EventOuterClass.Event> = Mono.fromCallable {
        log.debug("Space with id $id removed")
        spaceRemoved(correlationId, id)
    }

    companion object {
        private val log = logger<SpaceSaveListener>()
    }
}
