package pl.jdudycz.eci.store.space.messaging

import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactor.mono
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Space.SpaceData
import pl.jdudycz.eci.common.domain.event.EventOuterClass
import pl.jdudycz.eci.common.domain.event.spaceSaveFailed
import pl.jdudycz.eci.common.domain.event.spaceSaveSuccess
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.toUUID
import pl.jdudycz.eci.store.space.model.Space
import pl.jdudycz.eci.store.space.persistence.SpaceRepository
import pl.jdudycz.eci.store.space.persistence.transactions.SpaceCreateTransaction
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*
import javax.annotation.PostConstruct

@Service
class SpaceSaveListener(@Qualifier("spaceSave") private val consumer: EventConsumer,
                        private val publisher: EventPublisher,
                        private val spaceCreate: SpaceCreateTransaction,
                        private val spaceRepository: SpaceRepository) {

    @PostConstruct
    fun listen() {
        consumer.consume().flatMap(::handleSpaceCreate).subscribe()
    }

    private fun handleSpaceCreate(event: EventOuterClass.Event): Mono<Void> =
            Mono.fromCallable { SpaceData.parseFrom(event.data) }
                    .flatMap { save(event.correlationId.toUUID(), it) }

    private fun save(correlationId: UUID, data: SpaceData): Mono<Void> =
            spaceRepository
                    .verifyNameNotTaken(data.name)
                    .then(if (data.id.isEmpty()) createSpace(data) else updateSpace(data))
                    .map {
                        log.debug("Space \"${it.name}\" created")
                        spaceSaveSuccess(correlationId, it.id!!, data)
                    }
                    .onErrorResume {
                        log.debug("Failed to create space: ${it.localizedMessage}")
                        spaceSaveFailed(correlationId, data, it.localizedMessage).toMono()
                    }
                    .transform(publisher::publish)
                    .then()

    private fun updateSpace(data: SpaceData) =
            spaceRepository.findById(data.id)
                    .map { Space(it.id, data.name, it.agentRegistrationCode) }
                    .flatMap { spaceRepository.save(it) }

    private fun createSpace(data: SpaceData) =
            generateRegistrationCode()
                    .map { Space(data.name, it) }
                    .flatMap { spaceCreate.create(it, data.principal) }

    private fun generateRegistrationCode(): Mono<String> = mono {
        var code = generateCode()
        val exists = suspend { spaceRepository.existsByAgentRegistrationCode(code).awaitSingle() }
        while (exists()) {
            code = generateCode()
        }
        code
    }

    private fun generateCode(): String = RandomStringUtils.randomAlphanumeric(REGISTRATION_CODE_LENGTH)

    companion object {
        private val log = logger<SpaceSaveListener>()
        private const val REGISTRATION_CODE_LENGTH = 16
    }
}
