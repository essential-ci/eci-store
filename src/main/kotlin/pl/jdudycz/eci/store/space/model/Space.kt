package pl.jdudycz.eci.store.space.model

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Space

@Document
data class Space(
        var id: String?,
        val name: String,
        val agentRegistrationCode: String,
) {
    constructor(name: String, code: String) : this(null, name, code)
}
