package pl.jdudycz.eci.store.space.model

import pl.jdudycz.eci.common.domain.Space

enum class SpaceRole {
    ADMIN,
    DEVELOPER;

    companion object {
        fun valueOf(role: Space.SpaceRole) = valueOf(role.toString())
    }
}
