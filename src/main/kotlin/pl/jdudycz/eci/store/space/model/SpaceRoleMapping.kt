package pl.jdudycz.eci.store.space.model

import pl.jdudycz.eci.common.domain.Space

data class SpaceRoleMapping(val userId: String, val spaceId: String, val role: SpaceRole) {

    constructor(data: Space.SpaceRoleMapping) : this(data.userId, data.spaceId, SpaceRole.valueOf(data.role))
}
