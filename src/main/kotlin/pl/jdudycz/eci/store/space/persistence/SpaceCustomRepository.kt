package pl.jdudycz.eci.store.space.persistence

import reactor.core.publisher.Mono

interface SpaceCustomRepository {

    fun verifyExistsById(id: String): Mono<Void>

    fun verifyNameNotTaken(name: String): Mono<Void>
}
