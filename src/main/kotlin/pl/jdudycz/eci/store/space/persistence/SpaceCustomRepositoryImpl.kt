package pl.jdudycz.eci.store.space.persistence

import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
class SpaceCustomRepositoryImpl(@Lazy private val spaceRepository: SpaceRepository) : SpaceCustomRepository {

    override fun verifyExistsById(id: String): Mono<Void> =
            spaceRepository.existsById(id)
                    .doOnNext { if (!it) throw Exception("Space with id $id doesn't exist") }
                    .then()

    override fun verifyNameNotTaken(name: String): Mono<Void> =
            spaceRepository.existsByName(name)
                    .doOnNext { if (it) throw Exception("Space with name \"${name}\" already exists") }
                    .then()
}
