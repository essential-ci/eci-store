package pl.jdudycz.eci.store.space.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.space.model.Space
import reactor.core.publisher.Mono

@Repository
@Suppress("SpringDataRepositoryMethodReturnTypeInspection")
interface SpaceRepository : ReactiveCrudRepository<Space, String>, SpaceCustomRepository {

    fun existsByName(name: String): Mono<Boolean>

    fun findByAgentRegistrationCode(code: String): Mono<Space>

    fun existsByAgentRegistrationCode(code: String): Mono<Boolean>
}
