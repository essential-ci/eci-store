package pl.jdudycz.eci.store.space.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import pl.jdudycz.eci.store.space.model.SpaceRoleMapping
import reactor.core.publisher.Mono

@Repository
interface SpaceRoleMappingRepository : ReactiveCrudRepository<SpaceRoleMapping, String> {

    fun findBySpaceIdAndUserId(spaceId: String, userId: String): Mono<SpaceRoleMapping>

    fun deleteBySpaceIdAndUserId(spaceId: String, userId: String): Mono<Void>

    fun deleteBySpaceId(spaceId: String): Mono<Void>
}
