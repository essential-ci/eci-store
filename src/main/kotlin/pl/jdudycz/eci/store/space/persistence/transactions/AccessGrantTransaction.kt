package pl.jdudycz.eci.store.space.persistence.transactions

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.space.model.SpaceRoleMapping
import pl.jdudycz.eci.store.space.persistence.SpaceRoleMappingRepository

@Service
class AccessGrantTransaction(private val roleRepository: SpaceRoleMappingRepository) {

    @Transactional
    fun grantAccess(roleMapping: SpaceRoleMapping) =
            roleRepository
                    .deleteBySpaceIdAndUserId(roleMapping.spaceId, roleMapping.userId)
                    .then(roleRepository.save(roleMapping))
}
