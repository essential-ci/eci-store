package pl.jdudycz.eci.store.space.persistence.transactions

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.space.model.Space
import pl.jdudycz.eci.store.space.model.SpaceRole
import pl.jdudycz.eci.store.space.model.SpaceRoleMapping
import pl.jdudycz.eci.store.space.persistence.SpaceRepository
import pl.jdudycz.eci.store.space.persistence.SpaceRoleMappingRepository
import reactor.core.publisher.Mono

@Service
class SpaceCreateTransaction(private val spaceRepository: SpaceRepository,
                             private val roleRepository: SpaceRoleMappingRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun create(space: Space, creatorId: String): Mono<Space> =
            spaceRepository.save(space)
                    .flatMap { roleRepository.save(SpaceRoleMapping(creatorId, it.id!!, SpaceRole.ADMIN)) }
                    .map { space }
}
