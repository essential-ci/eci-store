package pl.jdudycz.eci.store.space.persistence.transactions

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.jdudycz.eci.store.project.persistence.ProjectRemoveTransaction
import pl.jdudycz.eci.store.project.persistence.ProjectRepository
import pl.jdudycz.eci.store.space.persistence.SpaceRepository
import pl.jdudycz.eci.store.space.persistence.SpaceRoleMappingRepository
import reactor.core.publisher.Mono

@Service
class SpaceRemoveTransaction(private val projectRemove: ProjectRemoveTransaction,
                             private val projectRepository: ProjectRepository,
                             private val spaceRepository: SpaceRepository,
                             private val roleRepository: SpaceRoleMappingRepository) {

    @Transactional(rollbackForClassName = ["java.lang.Exception"])
    fun remove(spaceId: String): Mono<Void> =
            projectRepository.findAllBySpaceId(spaceId)
                    .flatMap { projectRemove.remove(it.id!!) }
                    .then(roleRepository.deleteBySpaceId(spaceId))
                    .then(spaceRepository.deleteById(spaceId))
}
